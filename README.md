# Windows Serial Communication

Windows serial communication overlapped I/O speed test

## Setup
* Connect pin 3 to 2 (TXD to RXD) to perform loopback tests (to receive written bytes by self)
* Set minimal Latency Timer (e.g., 1 ms) in advanced settings of COM port driver
