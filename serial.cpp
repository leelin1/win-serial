#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <map>
#include <stdint.h>
#include <memory>
#include <filesystem>

#define UNICODE
#include "Windows.h"

using namespace std;

namespace serial
{
	static const std::string CR = "\x0d";
	static const std::string CRLF = "\x0d\x0a";
	static constexpr size_t InputBufferLength = 1024;

    static map<wstring, BYTE> PARITY_NAMES = {
        {L"none", 0},
        {L"odd", 1},
        {L"even", 2},
        {L"mark", 3},
        {L"space", 4}
    };

    const float STOP_BITS[] = { 1.0, 1.5, 2.0 };

	string hexDump(const char* pt_, unsigned int length)
	{
        ostringstream strm;
        ostringstream readable;
        const unsigned char* pt = (unsigned char*)pt_;
        strm << hex << std::setfill('0');
        size_t i;
        for (i = 0; i < length; i++)
        {
            if (i % 16 == 0) strm << setw(4) << i << "     ";
            strm << setw(2) << (unsigned int)pt[i] << ' ';
            if (pt[i] <= 31 || pt[i] >= 127)
            {
                readable << ' '; // '\xff';
            }
            else
            {
                readable << pt[i];
            }
            if (i % 8 == 7)
            {
                strm << "   ";
                readable << "  ";
            }
            if (i % 16 == 15)
            {
                strm << "  " << readable.str() << endl;
                readable.str("");
            }
        }
        if (readable.str().size())
        {
            static const string FILLER = ".. .. .. .. .. .. .. ..    .. .. .. .. .. .. .. ..";
            const auto n_ = readable.str().size();
            if (n_ <= 8)
            {
                strm << FILLER.substr(n_ * 3, string::npos);
            }
            else
            {
                strm << FILLER.substr(n_ * 3 - 3, string::npos);
            }
            strm << ' ';
            strm << "     " << readable.str() << endl;
        }
        return strm.str();
	}

    __int64 getSystemTime()
    {
        // Get high precision system time in 0.1 microseconds (us).
        FILETIME filetime;
        GetSystemTimePreciseAsFileTime(&filetime);
        ULARGE_INTEGER systime;
        systime.LowPart = filetime.dwLowDateTime;
        systime.HighPart = filetime.dwHighDateTime;
        return __int64(systime.QuadPart);
    }

    __int64 getTimeElapsedFromTicks(const void* start, const void* end, const void* frequency)
    {
        LARGE_INTEGER ElapsedMicroseconds;
        ElapsedMicroseconds.QuadPart = ((LARGE_INTEGER*)end)->QuadPart - ((LARGE_INTEGER*)start)->QuadPart;
        ElapsedMicroseconds.QuadPart *= 1000000;
        ElapsedMicroseconds.QuadPart /= ((LARGE_INTEGER*)frequency)->QuadPart;
        return __int64(ElapsedMicroseconds.QuadPart);
    }

	class SerialPort
	{
		HANDLE hComm = INVALID_HANDLE_VALUE;
		std::wstring mPortName;
		std::string mTerminator = CR;
		int mBaudrate = -1;
        bool isDebug = false;

	public:
        string terminator()
        {
            return mTerminator;
        }

        void setDebug(bool debug)
        {
            isDebug = debug;
        }

        bool isOpen()
        {
            return (this->hComm != INVALID_HANDLE_VALUE);
        }

        void open(const wstring& PortName)
        {
            mPortName = PortName;
            wcout << L"Connecting serial port " << PortName.c_str() << "...";

            string p;
            // Open serial port using WIN API
            this->hComm = CreateFile(PortName.c_str(),
                GENERIC_READ | GENERIC_WRITE,
                0,
                0,
                OPEN_EXISTING,
                FILE_FLAG_OVERLAPPED,
                //FILE_ATTRIBUTE_NORMAL,
                0);
            if (this->hComm == INVALID_HANDLE_VALUE)
            {
                // error opening port; abort
                cout << " INVALID_HANDLE_VALUE" << endl;
            }
            else
            {
                wcout << endl << "COM port opened: " << this->mPortName << endl;
            }
        }

        bool open(const wstring& PortName, DCB* dcb)
        {
            this->open(PortName);
            if (!this->isOpen())
            {
                return false;
            }

            DCB dcbActive = { 0 };
            dcbActive.DCBlength = sizeof(dcbActive);
            if (!GetCommState(this->hComm, &dcbActive)) {
                //error getting state
            }
            dcbActive.BaudRate = dcb->BaudRate;
            dcbActive.ByteSize = dcb->ByteSize;
            dcbActive.StopBits = dcb->StopBits;
            dcbActive.Parity = dcb->Parity;
            this->mBaudrate = dcbActive.BaudRate;

            BOOL fSuccess = SetCommState(this->hComm, &dcbActive);
            if (!fSuccess)
            {
                //  Handle the error.
                printf("SetCommState failed with error %d.\n", GetLastError());
                this->close();
                return false;
            }

            COMMTIMEOUTS timeouts = { 0 };
            timeouts.ReadIntervalTimeout = MAXDWORD;
            timeouts.ReadTotalTimeoutConstant = 0;
            timeouts.ReadTotalTimeoutMultiplier = 0;
            timeouts.WriteTotalTimeoutConstant = 1500;
            timeouts.WriteTotalTimeoutMultiplier = 0;
            if (!SetCommTimeouts(this->hComm, &timeouts)) {
                cout << "Error setting timeouts for COM port" << endl;
                this->close();
                return false;
            }

            BOOL setcomm;
            setcomm = EscapeCommFunction(this->hComm, SETRTS);
            if (!setcomm)
            {
                cout << "Error setting RTS" << endl;
                this->close();
                return false;
            }
            setcomm = EscapeCommFunction(this->hComm, SETDTR);
            if (!setcomm)
            {
                cout << "Error setting DTR" << endl;
                this->close();
                return false;
            }

            return true;
        }

        void write(const char* buffer, size_t length)
        {
            if (this->hComm == INVALID_HANDLE_VALUE) throw runtime_error("Invalid COM port handle");

            LARGE_INTEGER StartingTime, EndingTime;
            LARGE_INTEGER Frequency;
            QueryPerformanceFrequency(&Frequency);
            QueryPerformanceCounter(&StartingTime);

            DWORD bytesWritten;
            // Write to serial port in overlapped mode
            PurgeComm(this->hComm, PURGE_RXABORT | PURGE_RXCLEAR); // Clear input of last conversation.
            OVERLAPPED osWrite = { 0 };
            DWORD dwRes;
            BOOL fRes;
            osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
            if (!WriteFile(this->hComm, buffer, DWORD(length), &bytesWritten, &osWrite))
            {
                if (GetLastError() != ERROR_IO_PENDING)
                {
                    cout << "WriteFile failed, but isn't delayed. Report error and abort, err = " << GetLastError() << endl;
                    fRes = FALSE;
                }
                else
                {
                    // Write is pending.
                    dwRes = WaitForSingleObject(osWrite.hEvent, INFINITE);
                    switch (dwRes)
                    {
                    case WAIT_OBJECT_0:
                        if (!GetOverlappedResult(hComm, &osWrite, &bytesWritten, FALSE))
                            fRes = FALSE;
                        else
                            fRes = TRUE;
                        break;

                    default:
                        // An error has occurred in WaitForSingleObject.
                        // This usually indicates a problem with the
                        // OVERLAPPED structure's event handle.
                        cout << "An error has occurred in WaitForSingleObject." << endl;
                        fRes = FALSE;
                        break;
                    }
                }
            }
            else
            {
                // WriteFile completed immediately.
            }
            CloseHandle(osWrite.hEvent);

            if (isDebug)
            {
                QueryPerformanceCounter(&EndingTime);
                cout << hexDump((char*)buffer, bytesWritten);
                __int64 elapsed = getTimeElapsedFromTicks(&StartingTime, &EndingTime, &Frequency);
                cout << "OUT: " << bytesWritten << " chars (" << float(elapsed) / 1000 << " ms)" << endl;
            }

            if (length > bytesWritten)
            {
                cout << "Serial write incomplete." << endl;
            }
        }

        int read(string& input, int timeout = 1500)
        {
            LARGE_INTEGER StartingTime, EndingTime;
            LARGE_INTEGER Frequency;
            QueryPerformanceFrequency(&Frequency);
            QueryPerformanceCounter(&StartingTime);

            DWORD dwCommEvent = 0;
            input.resize(InputBufferLength);
            char* pt = (char*)input.c_str();
            int len = 0;
            DWORD dwRead;
            bool eof = false;
            const static size_t termLen = mTerminator.size();

            // Read input buffer using Windows API in overlapped mode.
            // Overlapped mode is required to handle the case that a DTE has no response at all
            // (not even the termination characters: 0x0d or 0x0d+0x0a).
            __int64 tTimeout = getSystemTime() + timeout * 10000;
            string buffer;
            buffer.resize(InputBufferLength);
            OVERLAPPED osReader;
            while (true)
            {
                osReader = { 0 };
                BOOL fWaitingOnRead = FALSE;
                osReader.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
                if (!fWaitingOnRead)
                {
                    if (!ReadFile(this->hComm, (char*)buffer.c_str(), DWORD(buffer.size()), &dwRead, &osReader)) {
                        if (GetLastError() != ERROR_IO_PENDING)     // read not delayed?
                        {
                            cout << "Error in communications; report it." << endl;
                        }
                        else
                        {
                            fWaitingOnRead = TRUE;
                        }
                    }
                    else
                    {
                        // read completed immediately
                        memcpy(pt + len, buffer.c_str(), dwRead);
                        len += dwRead;
                        if (termLen && len >= termLen && memcmp(pt + len - termLen, mTerminator.c_str(), termLen)==0)
                        {
                            if (isDebug) cout << "EOF" << endl;
                            eof = true;
                            break;
                        }
                    }
                }
                if (fWaitingOnRead)
                {
                    DWORD dwRes = WaitForSingleObject(osReader.hEvent, 50);
                    switch (dwRes)
                    {
                        // Read completed.
                        case WAIT_OBJECT_0:
                            if (!GetOverlappedResult(this->hComm, &osReader, &dwRead, FALSE))
                            {
                                cout << "Error in communications; report it." << endl;
                            }
                            else
                            {
                                // Read completed successfully.
                                memcpy(pt + len, buffer.c_str(), dwRead);
                                len += dwRead;
                                if (termLen && len >= termLen && memcmp(pt + len - termLen, mTerminator.c_str(), termLen)==0)
                                {
                                    // Termination character(s) are detected
                                    if (isDebug) cout << "EOF" << endl;
                                    eof = true;
                                }
                            }
                            fWaitingOnRead = FALSE;
                            //  Reset flag so that another opertion can be issued.
                            break;

                        case WAIT_TIMEOUT:
                            // Operation isn't complete yet. fWaitingOnRead flag isn't
                            // changed since I'll loop back around, and I don't want
                            // to issue another read until the first one finishes.
                            //
                            // This is a good time to do some background work.
                            break;

                        default:
                            // Error in the WaitForSingleObject; abort.
                            // This indicates a problem with the OVERLAPPED structure's
                            // event handle.
                            break;
                    }
                }
                if (eof) break;
                if (getSystemTime() > tTimeout)
                {
                    if (isDebug) cout << "TIMEOUT" << endl;
                    break;
                }
                CloseHandle(osReader.hEvent);
            }
            CloseHandle(osReader.hEvent);

            string debug(input);
            debug.resize((len > termLen) ? len - termLen : 0);
            if (isDebug)
            {
                QueryPerformanceCounter(&EndingTime);
                if (len)
                {
                    cout << hexDump((char*)pt, len);
                }
                __int64 elapsed = getTimeElapsedFromTicks(&StartingTime, &EndingTime, &Frequency);
                cout << "IN : " << len << " chars (" << float(elapsed) / 1000 << " ms)" << endl;
            }
            return len;
        }

        void close()
        {
            if (this->isOpen())
            {
                CloseHandle(this->hComm);
                this->hComm = INVALID_HANDLE_VALUE;
                wcout << "Serial port closed " << this->mPortName << endl;
                cout << endl;
            }
        }

        ~SerialPort()
        {
            this->close();
        }
    };   // End of class SerialPort
} // End of namespace serial

int main()
{
    wstring portname;
    DCB dcb;
    dcb.BaudRate = 115200;
    dcb.ByteSize = 8;
    dcb.Parity = 0;
    dcb.StopBits = 1;
    float databits = 10;

    wstring line;
    wifstream fs(L"settings.txt");
    if (fs.is_open())
    {
        getline(fs, portname);
        wcout << "port = " << portname << '\n';
        if (getline(fs, line))
        {
            dcb.BaudRate = stoi(line);
            wcout << "baudrate = " << dcb.BaudRate << '\n';
        }
        if (getline(fs, line))
        {
            dcb.ByteSize = stoi(line);
            databits = float(dcb.ByteSize) + 2;    // 1 start bit and 1 parity bit
            wcout << "databits = " << dcb.ByteSize << '\n';
        }
        if (getline(fs, line))
        {
            dcb.Parity = serial::PARITY_NAMES[line];
            if (dcb.Parity == 0) databits -= 1.0;
            wcout << "parity = " << dcb.Parity << '\n';
        }
        if (getline(fs, line))
        {
            dcb.StopBits = stoi(line);
            databits += serial::STOP_BITS[dcb.StopBits];
            wcout << "stopbits = " << dcb.StopBits << '\n';
        }
        fs.close();
    }
    else
    {
        cout << "settings.txt not found" << endl;
    }

    __int64 elapsed;

    serial::SerialPort port;
    port.setDebug(true);
    port.open(portname, &dcb);
    string data = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0" + port.terminator();
    string read;

    cout << endl;
    port.write(data.c_str(), data.length());
    port.read(read);
    cout << endl;

    port.setDebug(false);

    size_t bytesSent, bytesRead;
    __int64 t0;
    float bitrate;
    int iterations = 0;

    cout << "Testing with 64 chars..." << endl;
    bytesSent = 0;
    bytesRead = 0;
    elapsed = 0;
    t0 = serial::getSystemTime();
    while (elapsed < 2000)
    {
        port.write(data.c_str(), data.length());
        bytesSent += data.length();
        bytesRead += port.read(read);
        const auto t1 = serial::getSystemTime();
        elapsed = (t1 - t0) / 10000;
        iterations++;
        if (strncmp(data.c_str(), read.c_str(), data.size())) cout << "Data corrupted! Sent: " << data.length() << ", received: " << read.length() << "." << endl;
    }
    cout << "Time elapsed: " << float(elapsed) << " ms (" << bytesSent << " bytes sent)" << endl;
    bitrate = (float(bytesSent) * 10 / (float(elapsed) / 1000));

    const auto bytes1 = bytesSent;
    const auto elapsed1 = elapsed;

    cout << "Testing with 1 char..." << endl;
    data = port.terminator();
    bytesSent = 0;
    bytesRead = 0;
    elapsed = 0;
    t0 = serial::getSystemTime();
    for (int i = 0; i < iterations; i++)
    {
        port.write(data.c_str(), data.length());
        bytesSent += data.length();
        bytesRead += port.read(read);
        if (strncmp(data.c_str(), read.c_str(), data.size())) cout << "Data corrupted! Sent: " << data.length() << ", received: " << read.length() << "." << endl;
    }
    const auto t1 = serial::getSystemTime();
    elapsed = (t1 - t0) / 10000;
    cout << "Time elapsed: " << float(elapsed) << " ms (" << iterations << " operations)" << endl;

    bitrate = (float(bytes1 - bytesSent) * databits / (float(elapsed1 - elapsed) / 1000));
    cout << "Data rate: " << fixed << bitrate << " bps (" << (bitrate / dcb.BaudRate * 100) << "%)" << endl;
    cout << "Base latency: " << float(elapsed) / iterations << " ms" << endl;
    cout << endl;

    system("pause");
}